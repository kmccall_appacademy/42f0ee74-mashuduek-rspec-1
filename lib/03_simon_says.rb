def shout(string)
  string = string.upcase
end

def echo(string)
  string
end


def repeat(string, num = 2)
  i = 0
  array = []
  while i < num
    array.push(string)
    i += 1
  end
  array.join(' ')
end

def start_of_word(string, num)

  output = []
  i = 0
  while i < num
    output.push(string[i])
    i += 1
  end
  output.join
end

def first_word(string)
  array = string.split
  array.first
end

def titleize(words)
  little_words = ['and', 'or', 'the', 'over']
  array = words.split
  new_arr = []

  array.each do |word|
    unless little_words.include?(word)
      word.capitalize!
    end
    new_arr.push(word)
  end
  new_arr[0].capitalize!
  new_arr.join(' ')
end
