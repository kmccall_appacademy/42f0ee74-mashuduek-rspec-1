def ftoc(degree)
  celcius = (degree - 32) * (5.to_f/9)
  return celcius
end

def ctof(degree)
  farenheit = (degree * (9.to_f/5)) + 32
  return farenheit
end
