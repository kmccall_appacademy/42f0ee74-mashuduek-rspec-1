def add(num1, num2)
  return num1 + num2
end

def subtract(num1, num2)
  return num1 - num2
end

def sum(array)
  i = 0
  count = 0
  while i < array.length
    count += array[i]
    i += 1
  end
  count
end

def multiply(*nums)
  nums.reduce(:*)
end

def power (num1, num2)
  return num1 ** num2
end

def factorial(num)
  i = 1
  return 0 if num < i
  count = num
  while i < num
    count *= (num - i)
    i += 1
  end
  count
end
