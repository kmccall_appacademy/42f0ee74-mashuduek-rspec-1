def translate_word(string)

  vowels = ['a', 'e', 'i', 'o',' u']
  str_array = string.split(//)

  if vowels.include?(string[0])
    return string << 'ay'
  else

    while !vowels.include?(str_array[0])

      current = str_array[0]
      str_array.delete(str_array[0])
      str_array << current

      if str_array[0] == 'q'
        temp = str_array[0]
        temp1 = str_array[1]
        str_array.delete(str_array[1])
        str_array.delete(str_array[0])
        str_array << temp
        str_array << temp1
      end
    end

    str_array << 'ay'
  end
  str_array.join
end

def translate(words)
  array = words.split
  new_arr = []
  array.each do |word|
      translated = translate_word(word)
      new_arr.push(translated)
  end

  new_arr.join(' ')
end
